#!/bin/bash

#check input
if (($# == 0))
then
	#Jika tidak ada masukkan input 1 1 1
	./kobeni_liburan.sh 1 1 1
else
	#Jika ada masukkan ke program
	x=$1
	y=$2
	z=$3

	#Buat variabel zipName
	zipName="devil_${x}"
	
	#Buat folder yang nantio akan dizip dan masuk ke dalamnya
	mkdir $zipName
	cd $zipName
	
	#Buat variabel berisi nama hari sekarang
	hari=$(date +%A)

	#Masuk ke loop selama hari masih sama
	while (("$(date +%A)" == "$hari"))
	do
		#Buat variabel nama folder untuk menyimpan gambar
		folderName="kumpulan_${y}"
		
		#Buat folder dan masuk ke dalamnya
		mkdir $folderName
		cd $folderName
		
		#Buat variabel berisi jam sekarang
		jam=$(date +%H)
		jam="${jam##*0}"
		
		#Masuk loop untuk mendownload sesuai dengan jam
		for((a=1; a<=$jam; a=$((a+1))))
		do
			#Mendownload file
			wget -O perjalanan_${z} "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTyoF0p6XXl5v-YNDsTx1crlkxynURXFyYpUw&usqp=CAU"
			z=$((z+1))
		done
		
		#Kembali ke folder sebelumnya
		cd ..
		y=$((y+1))
		
		#Lanjut setelah 10 jam
		sleep 10h
	done
	
	#kembali ke folder sebelumnya lalu zip folder
	cd ..
	zip -r "$zipName" "$zipName"
	rm -r "$zipName"
	x=$((x+1))
fi
#Setelah ganti hari, jalankan program lagi dengan variable x y z
./kobeni_liburan.sh $x $y $z
